package main

import (
	"os"
	"fmt"
	"os/exec"
	"syscall"
)

//docker run <container name> cmd args
// go run main.go run cmd args


func main() {
	switch os.Args[1] {
	case "run":
		run()
	case "child":
		child()
	default:
		panic ("WTF?")

	}
}

func run()  {
	//fmt.Printf("running %v\n", os.Args[2:])
	//folk exec 'proc/self/exec but we will need to put it as child
	cmd := exec.Command("/proc/self/exe", append([]string{"child"}, os.Args[2:]...)...)
	//cmd := exec.Command(os.Args[2], os.Args[3:]...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.SysProcAttr = &syscall.SysProcAttr {
		Cloneflags: syscall.CLONE_NEWUTS | syscall.CLONE_NEWPID,
	}

	errhandler(cmd.Run())
}

func child(){
	fmt.Printf("running %v as PID %d\n", os.Args[2:], os.Getpid())
	//cmd := exec.Command("/proc/self/exe", append([]string{"child"}, os.Args[2:]...)...)
	cmd := exec.Command(os.Args[2], os.Args[3:]...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	errhandler(syscall.Chroot("/home/parallels/rootfs_tar"))
	errhandler(syscall.Chdir("/"))
	errhandler(syscall.Mount("proc","proc","proc",0,""))

	errhandler(cmd.Run())
}

func errhandler(err error){
	if err!=nil {
		panic(err)
	}
}
